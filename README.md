# `html-template-generator` [![version](https://img.shields.io/github/release/hchiam/html-template-generator)](https://github.com/hchiam/html-template-generator/releases)

Generate HTML from a visual UI!

[Live demo](https://html-template-generator.surge.sh)

[Live editable demo](https://codepen.io/hchiam/pen/jOBOaqm)

## Local development

```bash
open index.html
```

or

```bash
yarn
yarn global add parcel-bundler
yarn start
# open http://localhost:1234/
```

(You'll need [`yarn`](https://github.com/hchiam/learning-yarn) set up. For deployment, I use [`surge`](https://github.com/hchiam/learning-surge).)
